# Handbook

## Functions, Processes, Projects, and Roles

In this document a function is something that given an input produces an output.
That something can be a mathematical formula, source code, or people carrying out defined tasks repeatedly.
Pure functions only depend on the inputs given directly.
For all practical purposes and within organizations, functions also depend on the context (acting as implicit input or state)
they are invoked within or have memorized from prior executions.
Sometimes such additional implicit inputs are named side effects.

\columns=40%,30%,

| Foo  | Bar | Baz |
|:-----|:---:|----:|
| Quux |  x  |   42|
| xQuu |  y  |   -1|
| uxQu |  z  | true|

Table: The old tune\label{tab:tuna}

Now look at tuna Table\ \ref{tab:tuna}

\columns=40%,30%,

| Foo  | Bar | Baz |
|:-----|:---:|----:|
| Quuxxxxxxxx xxxxxxxxxxxxx xxxxxxxxxxxxx xxxxxxxxxxx xxxxxxxxxx |  xxxxxxx xxxxxxxxxxxxxxxxxxxx xxxxxxxxxxxx xxxxxxxx  |   42|
| xQuu |  y  |   -1|
| uxQu |  z  | true|

Table: The wide tune\label{tab:wuna}

Now look at wuna Table\ \ref{tab:wuna}
