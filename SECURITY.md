# Security Policy

## Supported Versions

Versions of liitos currently being supported with security updates.

| Version(s) | Supported |
|:-----------|:----------|
| 2025.1.5   | yes       |
| < 2025.1.5 | no        |

## Reporting a Vulnerability

Please contact the maintainer per stefan@hagen.link
